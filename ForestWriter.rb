#!/usr/bin/ruby

require 'RMagick'
include Magick
include Math

class Forest
    def initialize(logo_filename)
        @logo_filename = logo_filename
    end

    def get_forest_image()
        logo = Image.read(@logo_filename)[0]
        logo_width = logo.columns
        logo_height = logo.rows

        green_tree = Image.read('tree12.png')[0]
        grey_tree = Image.read('tree22.png')[0]
        tree_width = green_tree.columns

        forest_image = Image.new(logo_width * tree_width, logo_height * tree_width)

        (0..logo_height - 1).each do |row|
            greyscale_pixels = logo.export_pixels(0, row, logo_width, 1, "I").map {|value| value / 65535.0}

            (0..logo_width - 1).each do |column|
                tree = if greyscale_pixels[column] > 0.8 then green_tree else grey_tree end
                forest_image.composite!(tree, column * tree_width, row * tree_width, CopyCompositeOp)
            end
        end

        return forest_image
    end

    def output_whole_forest(output_filename)
        get_forest_image().write(output_filename)
    end
end


class Deep_Zoom
    def initialize(forest, tile_size, overlap, output_format)
        @forest = forest
        @tile_size = tile_size
        @overlap = overlap
        @output_format = output_format
    end

    def export(output_path, dzi_name)
        Dir.mkdir(output_path) if !Dir.exists?(output_path)

        dzi_filename = File.join(output_path, dzi_name + ".dzi")
        dzi_image_path = File.join(output_path, dzi_name + "_files")

        if File.exists?(dzi_filename) || Dir.exists?(dzi_image_path) then
            puts 'DeepZoom files already exist ... exiting'
            abort
        end

        image = @forest.get_forest_image()
        output_dzi_file(dzi_filename, image)

        Dir.mkdir(dzi_image_path)
        Dir.chdir(dzi_image_path) do
            get_maximum_zoom_level(image).downto 0 do |zoom_level|
                n_tiles_x = (image.columns.to_f / @tile_size).ceil
                n_tiles_y = (image.rows.to_f / @tile_size).ceil
                n_tiles = n_tiles_x * n_tiles_y

                puts "level #{zoom_level} is #{image.columns} x #{image.rows} (#{n_tiles} tiles)"

                Dir.mkdir(zoom_level.to_s)
                Dir.chdir(zoom_level.to_s) { generate_tiles(image) }

                #Halve the image dimensions after each iteration
                image.resize!((image.columns / 2.0).ceil, (image.rows / 2.0).ceil)
            end
        end
    end

    def output_dzi_file(output_file, image)
        xml = [
            "<?xml version=\"1.0\" encoding=\"UTF-8\"?>",
            "<Image TileSize=\"#{@tile_size}\" Overlap=\"#{@overlap}\" Format=\"#{@output_format}\"",
            " xmlns=\"http://schemas.microsoft.com/deepzoom/2008\">",
            "<Size Width=\"#{image.columns}\" Height=\"#{image.rows}\" />",
            "</Image>"
        ]

        File.open(output_file, 'w') { |file| file.write(xml.join("\n")) }
    end

    def get_maximum_zoom_level(image)
        longest_dimen = [image.columns, image.rows].max
        Math.log(longest_dimen, 2).ceil
    end

    def generate_tiles(image)
        (0..image.columns).step(@tile_size).each do |x|
            (0..image.rows).step(@tile_size).each do |y|
                left = [x - @overlap, 0].max
                top = [y - @overlap, 0].max
                right = [x + @tile_size + @overlap, image.columns].min
                bottom = [y + @tile_size + @overlap, image.rows].min
                width = right - left
                height = bottom - top

                tile_x = x / @tile_size
                tile_y = y / @tile_size

                tile = image.crop(left, top, width, height)
                tile.write("#{tile_x}_#{tile_y}.#{@output_format}")
                tile.destroy!
            end
        end
    end
end


forest = Forest.new('Barclays.jpg')
deep_zoom = Deep_Zoom.new(forest, 256, 2, 'jpg')
deep_zoom.export('output', 'barclays')
