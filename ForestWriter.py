#!/usr/bin/python

import Image, math, os

class Forest:
    def __init__(self, logoFilename):
        self.logoFilename = logoFilename

    def getForestImage(self):
        logo = Image.open(self.logoFilename).convert("L")  # Convert to greyscale
        logoWidth, logoHeight = logo.size

        greenTree = Image.open('tree12.png')
        greyTree = Image.open('tree22.png')

        treeWidth, treeHeight = greenTree.size

        forestImage = Image.new("RGB", (logoWidth * treeWidth, logoHeight * treeHeight))

        pixels = logo.load()
        for x in range(0, logoWidth):
            for y in range(0, logoHeight):
                if pixels[x,y] > 0.75 * 255:
                    tree = greenTree
                else:
                    tree = greyTree

                forestImage.paste(tree, (x * treeWidth, y * treeHeight))

        return forestImage

    def outputWholeForest(self, outputFilename):
        self.getForestImage().save(outputFilename, 'png')

class DZIFactory:
    def __init__(self, tileSize, overlap, outputFormat):
        self.tileSize = tileSize
        self.overlap = overlap
        self.outputFormat = outputFormat

    def generate(self, image, outputFolder, outputName):
        outputPath = self.getOrCreateDir(outputFolder)
        dziFilename = os.path.join(outputPath, outputName + '.dzi')
        dziImagePath = os.path.join(outputPath, outputName + '_files', '')

        if os.path.exists(dziFilename) or os.path.exists(dziImagePath):
            exit('DeepZoom files already exist')

        self.outputXmlFile(image, dziFilename)

        initialDir = os.getcwd()
        os.makedirs(dziImagePath)
        os.chdir(dziImagePath)

        for zoomLevel in range(self.getMaxZoomLevel(image), -1, -1):  # Max level down to and including zero
            nTilesX = math.ceil(image.size[0] / float(self.tileSize))
            nTilesY = math.ceil(image.size[1] / float(self.tileSize))
            nTiles = nTilesX * nTilesY
            print "level %i is %i x %i (%i tiles)" % (zoomLevel, image.size[0], image.size[1], nTiles)

            imageDir = os.getcwd()
            zoomPath = self.getOrCreateDir(os.path.join(str(zoomLevel), ''))
            os.chdir(zoomPath)
            self.generateTiles(image)
            os.chdir(imageDir)

            #Halve the image dimensions after each iteration
            image = image.resize((int(math.ceil(image.size[0] / 2.0)), int(math.ceil(image.size[1] / 2.0))), Image.ANTIALIAS)

        os.chdir(initialDir)

    def getOrCreateDir(self, pathName):
        outputPath = os.path.dirname(pathName)

        if not os.path.exists(outputPath):
            os.makedirs(outputPath)

        return outputPath


    def outputXmlFile(self, image, filename):
        xml = [
                '<?xml version="1.0" encoding="UTF-8"?>',
                '<Image TileSize="%i" Overlap="%i" Format="%s"' % (self.tileSize, self.overlap, self.outputFormat),
                ' xmlns="http://schemas.microsoft.com/deepzoom/2008">',
                '<Size Width="%i" Height="%i" />' % image.size,
                '</Image>',
                ''
            ]

        dziFile = open(filename, 'w')
        dziFile.write("\n".join(xml))
        dziFile.close()

    def getMaxZoomLevel(self, image):
        return int(math.ceil(math.log(max(image.size), 2)))

    def generateTiles(self, image):
        for x in range(0, image.size[0], self.tileSize):
            for y in range(0, image.size[1], self.tileSize):
                left = max(x - self.overlap, 0)
                top = max(y - self.overlap, 0)
                right = min(x + self.tileSize + self.overlap, image.size[0])
                bottom = min(y + self.tileSize + self.overlap, image.size[1])

                tile = image.crop((left, top, right, bottom))

                tileX = x / self.tileSize
                tileY = y / self.tileSize

                tile.save("%i_%i.%s" % (tileX, tileY, self.outputFormat))

forest = Forest('Barclays.jpg')
dziFactory = DZIFactory(256, 2, 'jpg')
dziFactory.generate(forest.getForestImage(), 'output/', 'python')
